﻿if (!!window.Type) {
    //this is "normal"
    Type.registerNamespace("SpSyntaxHighlighter");
} else {
    //this happend on i.e. EditBlog.aspx
    window.SpSyntaxHighlighter = window.SpSyntaxHighlighter || {};
}
SpSyntaxHighlighter.canApplySyntaxHighlighter = function () {
    /// <summary>Check, if SyntaxHighlighter should be applied. I.e. returns false, if something is in edit mode..</summary>
    /// <returns type="bool">fals, if loading of SyntaxHighlighter should be disabled</returns>
    var rteFields = document.getElementsByClassName('ms-rtefield');
    // check if page is in edit-mode or edit-form or something..
    // do not (!) modify source in an editor - because that will lead to the modification being saved...
    if (rteFields.length > 0) {
        SP.UI.Notify.addNotification('Edit-Mode detected. SyntaxHighlighter disabled.', false);
        return false;
    }
    return true;
};
SpSyntaxHighlighter.shAutoConfig = ['$shAutoConfig$'];
SpSyntaxHighlighter.runSyntaxHighlighter = function () {
    /// <summary>
    ///     Re-Run SyntaxHighlighter, make sure SyntaxHighlighter was loaded before calling.
    ///     i.e. call loadSyntaxHighlighter first.
    ///     this functions does nothing, if an RichTextEditor is opened in the page!
    /// </summary>

    if (!SpSyntaxHighlighter.canApplySyntaxHighlighter()) return;

    // restore the global SH-object if it was garbage-collected by MDS
    if (!window.SyntaxHighlighter) {
        window.SyntaxHighlighter = SpSyntaxHighlighter.SyntaxHighlighter;
    }
    window.SyntaxHighlighter.autoloader.apply(undefined, SpSyntaxHighlighter.shAutoConfig);
    window.SyntaxHighlighter.vars.discoveredBrushes = null; // let SyntaxHighlighter re-run brush-discovery
    window.SyntaxHighlighter.all();
    // There is something itchy in autoloader.. SyntaxHighlighter.all() can not be called twice, if SyntaxHighlighter.autoloader was loaded :-(
    //TODO: don't use autoloader...
    window.setTimeout(window.SyntaxHighlighter.highlight.bind(window.SyntaxHighlighter), 100);
};
SpSyntaxHighlighter.loadSyntaxHighlighter = function () {
    var csspath = '$cssPath$',
        scriptpath = '$scriptPath$',
        shTheme = '$theme$', //Possible Themes: $allThemes$
        addEvent = function (elm, evt, func) {
            if (elm.addEventListener) {
                elm.addEventListener(evt, func);
            } else if (elm.attachEvent) {
                elm.attachEvent('on' + evt, func);
            } else {
                elm['on' + evt] = func;
            }
        },
        removeEvent = function (elm, evt, func) {
            if (elm.removeEventListener) {
                elm.removeEventListener(evt, func);
            } else if (elm.detachEvent) {
                elm.detachEvent('on' + evt, func);
            } else {
                elm['on' + evt] = null;
            }
        },
        loadScript = function (url, successHandler, errorHandler) {
            var script = document.createElement('script'),
                loaded, error;
            script.src = url;
            script.type = 'text/javascript';
            script.language = 'javascript';
            loaded = function () {
                removeEvent(script, 'load', loaded);
                removeEvent(script, 'error', error);
                if (successHandler) {
                    successHandler(script);
                }
            };
            error = function () {
                removeEvent(script, 'load', loaded);
                removeEvent(script, 'error', error);
                if (errorHandler) {
                    errorHandler();
                }
            };

            addEvent(script, 'load', loaded);
            addEvent(script, 'error', error);
            document.body.appendChild(script);
        },
        loadCss = function (url) {
            var link = document.createElement('link');
            link.href = url;
            link.type = 'text/css';
            link.rel = 'stylesheet';
            document.head.appendChild(link);
        },
        loadSyntaxHighlighter = function () {
            // add SyntaxHighlighter, styles and autoloader, then run autoloader
            loadCss(csspath + 'shCore.css');
            loadCss(csspath + shTheme);
            loadScript(scriptpath + 'shCore.js', function () {
                SpSyntaxHighlighter.SyntaxHighlighter = window.SyntaxHighlighter; // store SyntaxHighlighter in namespace...
                loadScript(scriptpath + 'shAutoLoader.js', function () {
                    SpSyntaxHighlighter.runSyntaxHighlighter();
                });
            });
        },
        siteLoadFialed = function (err) {
            if (!!(console && console.log)) {
                console.log('accessing siteCollecion failed. ');
                console.log('err was: ' + err);
                console.log('execution aborted. :-(');
            }
            // this is the end of it. sadly.
        },
        siteLoadSucceded = function () {
            var siteCollectionUrl = siteCollection.get_url(),
                i, len, brushCfg, brushPath;
            // fix up paths...
            csspath = csspath.replace("~siteCollection", siteCollectionUrl);
            scriptpath = scriptpath.replace("~siteCollection", siteCollectionUrl);

            len = SpSyntaxHighlighter.shAutoConfig.length;
            for (i = 0; i < len; i += 1) {
                brushCfg = SpSyntaxHighlighter.shAutoConfig[i];
                brushPath = brushCfg[brushCfg.length - 1];
                brushCfg[brushCfg.length - 1] = brushPath.replace("~siteCollection", siteCollectionUrl);
            }

            // now start loading SyntaxHighlighter
            loadSyntaxHighlighter();
        },
        siteCollection,
        clientContext = SP.ClientContext.get_current(),
        highlightable = document.getElementsByTagName('pre');

    // check if syntaxHighlighter ist needed
    if (highlightable.length < 1) return;

    if (!SpSyntaxHighlighter.canApplySyntaxHighlighter()) return;

    // load siteCollectionData then continue
    siteCollection = clientContext.get_site();
    clientContext.load(siteCollection);
    clientContext.executeQueryAsync(siteLoadSucceded, siteLoadFialed);
};
SpSyntaxHighlighter.preLoadSyntaxHighlighter = function () {
    if (!!SpSyntaxHighlighter.SyntaxHighlighter) {
        //we've been here before :-)
        SpSyntaxHighlighter.runSyntaxHighlighter();
        return;
    }
    // load all from scratch...
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', SpSyntaxHighlighter.loadSyntaxHighlighter);
};
SpSyntaxHighlighter.register = function () {
    var thisUrl;
    SpSyntaxHighlighter.preLoadSyntaxHighlighter();
    if (!!window._spPageContextInfo) {
        thisUrl = '$shLoaderScriptUrl$'.replace('~siteCollection/', window._spPageContextInfo.siteServerRelativeUrl);
        window.RegisterModuleInit(thisUrl, SpSyntaxHighlighter.preLoadSyntaxHighlighter);
    }
};

//Simulate LoadAfterUi=true
window._spBodyOnLoadFunctionNames.push("SpSyntaxHighlighter.register");
